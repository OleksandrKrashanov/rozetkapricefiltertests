﻿using OpenQA.Selenium;

namespace RozetkaPriceFilterTests.Pages
{
    class HomePage : BasePage
    {
        private readonly By _notebooksAndComputersLink = By.XPath("//a[@class='menu-categories__link'][contains(@href,'computers-notebooks')]");
        public HomePage(IWebDriver driver) : base(driver) { }

        public void ClickOnNotebooksAndComputersLink()
        {
            driver.FindElement(_notebooksAndComputersLink).Click();
        }
    }
}
