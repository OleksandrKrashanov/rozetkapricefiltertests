﻿using OpenQA.Selenium;

namespace RozetkaPriceFilterTests.Pages
{
    class NotebooksAndComputersPage : BasePage
    {
        private readonly By _notebooksLink = By.XPath("//a[@class='tile-cats__picture'][contains(@title,'Ноутбуки')]");
        public NotebooksAndComputersPage(IWebDriver driver) : base(driver) { }

        public void ClickOnNotebooksLink()
        {
            driver.FindElement(_notebooksLink).Click();
        }

        public By GetNotebooksLinkLocator()
        {
            return _notebooksLink;
        }
    }
}
