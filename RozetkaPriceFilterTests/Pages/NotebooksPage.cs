﻿using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;

namespace RozetkaPriceFilterTests.Pages
{
    class NotebooksPage : BasePage
    {

        private readonly By _minPriceFilterInput = By.XPath("//input[@formcontrolname='min']");

        private readonly By _submitFilterButton = By.XPath("//button[contains(@type,'submit')]");

        private readonly By _productsPriceList = By.XPath("//span[@class='goods-tile__price-value']");
        public NotebooksPage(IWebDriver driver) : base(driver) { }

        public void ClearMinPriceFilterField()
        {
            driver.FindElement(_minPriceFilterInput).Clear();
        }

        public void InputDataToMinPriceFilterField(string minData)
        {
            driver.FindElement(_minPriceFilterInput).SendKeys(minData);
        }

        public void ClickOnSubmitFilterButton()
        {
            driver.FindElement(_submitFilterButton).Click();
        }

        public List<IWebElement> GetFirstSetOFProductsPriceList()
        {
            return driver.FindElements(_productsPriceList).Take(5).ToList();
        }

        public By GetMinPriceFilterLocator()
        {
            return _minPriceFilterInput;
        }

        public By GetSubmitFilterButton()
        {
            return _submitFilterButton;
        }

        public By GetProductsPriceListLocator()
        {
            return _productsPriceList;
        }
    }
}
