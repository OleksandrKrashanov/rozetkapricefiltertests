﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace RozetkaPriceFilterTests.Pages
{
    class BasePage
    {
        protected IWebDriver driver;

        public BasePage(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void WaitForElementIsVisible(TimeSpan timeToWait,By locator)
        {
            new WebDriverWait(driver, timeToWait).Until(ExpectedConditions.ElementIsVisible(locator));
        }
    }
}
