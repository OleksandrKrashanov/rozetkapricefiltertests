﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RozetkaPriceFilterTests.Tests
{
    class NegativePriceFilterTests : BaseTest
    {
        private const string CHARS = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@#%^&*()_+-=$?_-1234567890";
        private const int MAX_LENGTH = 20;
        private const int SECONDS = 30;
        private const int FIELD_MIN_VALUE = 50;

        [Test, Order(1)]
        public void CheckThatSubmitButtonIsNotClickableAfterInputRandomString()
        {
            GetHomePage().ClickOnNotebooksAndComputersLink();
            GetNotebooksAndComputersPage()
                .WaitForElementIsVisible(TimeSpan.FromSeconds(SECONDS), GetNotebooksAndComputersPage().GetNotebooksLinkLocator());

            GetNotebooksAndComputersPage().ClickOnNotebooksLink();
            GetNotebooksPage()
                .WaitForElementIsVisible(TimeSpan.FromSeconds(SECONDS), GetNotebooksPage().GetMinPriceFilterLocator());

            GetNotebooksPage().ClearMinPriceFilterField();
            GetNotebooksPage()
                .InputDataToMinPriceFilterField(GetNotValidRandomData()
                .GenerateNotValidRandomString(CHARS, MAX_LENGTH));

            Assert.IsFalse(GetDriver().FindElement(GetNotebooksPage().GetSubmitFilterButton()).Enabled);
        }

        [Test, Order(2)]
        public void CheckThatSubmitButtonIsNotClickableAfterInputRandomNotValidMinNumber()
        {
            GetHomePage().ClickOnNotebooksAndComputersLink();
            GetNotebooksAndComputersPage()
                .WaitForElementIsVisible(TimeSpan.FromSeconds(SECONDS), GetNotebooksAndComputersPage().GetNotebooksLinkLocator());

            GetNotebooksAndComputersPage().ClickOnNotebooksLink();
            GetNotebooksPage()
                .WaitForElementIsVisible(TimeSpan.FromSeconds(SECONDS), GetNotebooksPage().GetMinPriceFilterLocator());

            GetNotebooksPage().ClearMinPriceFilterField();
            GetNotebooksPage()
                .InputDataToMinPriceFilterField(GetNotValidRandomData()
                .GenerateNotValidRandomNumber(FIELD_MIN_VALUE));

            Assert.IsFalse(GetDriver().FindElement(GetNotebooksPage().GetSubmitFilterButton()).Enabled);
        }
    }
}
