﻿using NUnit.Framework;
using System;

namespace RozetkaPriceFilterTests.Tests
{
    class PositivePriceFilterTests : BaseTest
    {
        private const int MIN_VALUE = 50;
        private const int MAX_VALUE = 224670;
        private const int SECONDS = 30;

        [Test, Order(1)]
        public void CheckThatFirstSetOfSearchResultIncludedInFilterRange()
        {
            GetHomePage().ClickOnNotebooksAndComputersLink();
            GetNotebooksAndComputersPage()
                .WaitForElementIsVisible(TimeSpan.FromSeconds(SECONDS), GetNotebooksAndComputersPage().GetNotebooksLinkLocator());

            GetNotebooksAndComputersPage().ClickOnNotebooksLink();
            GetNotebooksPage()
                .WaitForElementIsVisible(TimeSpan.FromSeconds(SECONDS), GetNotebooksPage().GetMinPriceFilterLocator());

            GetNotebooksPage().ClearMinPriceFilterField();
            GetNotebooksPage()
                .InputDataToMinPriceFilterField(GetValidRandomData()
                .GenerateVaLidRandomValue(MIN_VALUE, MAX_VALUE));
            GetNotebooksPage().ClickOnSubmitFilterButton();
            GetNotebooksPage()
                .WaitForElementIsVisible(TimeSpan.FromSeconds(SECONDS), GetNotebooksPage().GetProductsPriceListLocator());

            foreach (var item in GetNotebooksPage().GetFirstSetOFProductsPriceList())
            {
                int price = int.Parse(item.Text.Replace(" ", ""));
                Assert.IsTrue(price >= MIN_VALUE && price <= MAX_VALUE);
            }
        }

        [Test, Order(2)]
        public void CheckThatSubmitButtonIsClickable()
        {
            GetHomePage().ClickOnNotebooksAndComputersLink();
            GetNotebooksAndComputersPage()
                .WaitForElementIsVisible(TimeSpan.FromSeconds(SECONDS), GetNotebooksAndComputersPage().GetNotebooksLinkLocator());

            GetNotebooksAndComputersPage().ClickOnNotebooksLink();
            GetNotebooksPage()
                .WaitForElementIsVisible(TimeSpan.FromSeconds(SECONDS), GetNotebooksPage().GetMinPriceFilterLocator());

            GetNotebooksPage().ClearMinPriceFilterField();
            GetNotebooksPage()
                .InputDataToMinPriceFilterField(GetValidRandomData()
                .GenerateVaLidRandomValue(MIN_VALUE, MAX_VALUE));

            Assert.IsTrue(GetDriver().FindElement(GetNotebooksPage().GetSubmitFilterButton()).Enabled);
        }
    }
}
