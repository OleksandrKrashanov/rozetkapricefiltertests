﻿using NUnit.Framework;
using OpenQA.Selenium;
using RozetkaPriceFilterTests.Pages;
using RozetkaPriceFilterTests.Data;

namespace RozetkaPriceFilterTests.Tests
{
    class BaseTest
    {
        private IWebDriver driver;
        private const string ROZETKA_URL = "https://rozetka.com.ua/ua/";

        [SetUp]
        public void TestsSetup()
        {
            driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(ROZETKA_URL);
        }

        [TearDown]
        public void TestsTearDown()
        {
            driver.Close();
        }

        public IWebDriver GetDriver()
        {
            return driver;
        }

        public HomePage GetHomePage()
        {
            return new HomePage(GetDriver());
        }
        public NotebooksAndComputersPage GetNotebooksAndComputersPage()
        {
            return new NotebooksAndComputersPage(GetDriver());
        }

        public NotebooksPage GetNotebooksPage()
        {
            return new NotebooksPage(GetDriver());
        }

        public ValidRandomData GetValidRandomData()
        {
            return new ValidRandomData();
        }

        public NotValidRandomData GetNotValidRandomData()
        {
            return new NotValidRandomData();
        }
    }
}
