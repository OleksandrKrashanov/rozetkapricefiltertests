﻿using System;
using System.Linq;

namespace RozetkaPriceFilterTests.Data
{
    class NotValidRandomData
    {
        public string GenerateNotValidRandomString(string chars, int maxLength)
        {
            Random random = new Random();

            int length = random.Next(0, maxLength);

            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public string GenerateNotValidRandomNumber(int maxValue)
        {
            var random = new Random();

            return random.Next(int.MinValue, maxValue).ToString();
        }
    }
}
