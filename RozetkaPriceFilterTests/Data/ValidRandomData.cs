﻿using System;

namespace RozetkaPriceFilterTests.Data
{
    class ValidRandomData
    {
        public string GenerateVaLidRandomValue(int minValue, int maxValue)
        {
            var random = new Random();

            return random.Next(minValue, maxValue).ToString();
        }
    }
}
